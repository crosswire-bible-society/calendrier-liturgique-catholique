#!/bin/bash
## Supprime espace entre les références.
#sed -ri 's/([1-3]) ([CRTJPMS])/\1\2/g' *.csv
#sed -ri 's# / ou# ou#g' *.csv
##Supprime des caractères indésirables dans les fichiers
#sed -i 's/rouge~rouge/rouge/g' *.csv
#sed -i 's/~rouge~rouge//g' *.csv
#sed -i 's/~rouge~rouge~rouge~rouge//g' *.csv
#sed -i 's/blanc~blanc/blanc/g' *.csv
#sed -i 's/~blanc~blanc//g' *.csv
#sed -r 's/~[a-z]*\"/\"/g' CalendrierLiturgique_2019.csv >test
#sed -ri 's/†/mort en /g' perpet.xml

killall xiphos
#./Gre2HEb.sh
#sed -i '1d' *.csv
##Noël
##Rameaux
##Pâques
##Créations des listes de jours
./csv_xlm.py
for FILE in *.xml ; do
cat debuttext.txt "$FILE" finxml.txt > /tmp/myTmpFile
mv /tmp/myTmpFile "$FILE"

##Convertion des références
./cal-litt.sh
#awk -f verse_links_nv.awk -v modtype="OSIS" -v debug="$DEBUG" $FILE >$FILE.out
done
mkdir -p sword/mods.d sword/modules/lexdict/zld/devotionals/perpet/

rename 's/CalendrierLiturgique_([0-9]*.*).xml.out/\1/g' *.out
for FILE in 20* ; do
imp2ld $FILE -o sword/modules/lexdict/zld/devotionals/perpet/$FILE -z
done
#xiphos &
#exit
##Pour ajouter les débuts et fin de fichier
#for i in *.sed
#do cat debuttext.txt "$i" finxml.txt > /tmp/myTmpFile
#mv /tmp/myTmpFile "$i"
#done
#awk -F';' '{ gsub("\",\"","\";\""); gsub("\"","",$NF); n=split($NF,ar," \/ "); for(i=1;i<=n;i++)printf("\t<bla>%s</bla>\n",ar[i])}' fichier


for i in `ls 20*`; do
touch $i.conf
echo "[freCalLitCat$i]" >>$i.conf
done
for FILE in *.conf ; do
cat "$FILE" debutconf.txt  > /tmp/myTmpFile
mv /tmp/myTmpFile "$FILE"
done
for i in `ls 20*`; do
sed -i "s#perpet/#&$i#" $i.conf
done
mv *.conf sword/mods.d
rm 20*
cp sword/mods.d/* ~/.sword/mods.d/
cp sword/modules/lexdict/zld/devotionals/perpet/* ~/.sword/modules/lexdict/zld/devotionals/perpet/



