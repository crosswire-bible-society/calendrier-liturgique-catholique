#!/bin/awk -f

#
## verse_link.awk - dominique@corbex.org - 2019
#
# This script will insert valid OSIS references to a given file
#
# options
#   modtype : TEI or OSIS (osisRef formatting differs)
#   debug   : > 0 to see error msgs printed to /dev/stderr

#
# Script history
#   1.0 Initial release

BEGIN{
## 1. Variables used for matching abbreviations with Osis names
# list of abbreviations
list["Gen"]="Ge Gn Gen"
list["Exod"]="Ex Exo Exod"
list["Lev"]="Le Lé Lv Lev Lév"
list["Num"]="No Nb Nom"
list["Deut"]="De Dt Deu Deut"
list["Josh"]="Jos"
list["Judg"]="Jg Jug"
list["Ruth"]="Ru Rut Ruth"
list["1Sam"]="1S 1Sa 1Sam"
list["2Sam"]="2S 2Sa 2Sam"
list["1Kgs"]="1R 1Ro 1Rois"
list["2Kgs"]="2R 2Ro 2Rois"
list["1Chr"]="1Ch 1Chr"
list["2Chr"]="2Ch 2Chr"
list["Ezra"]="Esd"
list["Neh"]="Ne Neh Né Néh"
list["Esth"]="Est Esth"
list["Job"]="Jb Job"
list["Ps"]="Ps Psa"
list["Prov"]="Pr Pro"
list["Eccl"]="Ec Ecc Eccl"
list["Song"]="Ca Ct"
list["Isa"]="Es És Esa Ésa Is Isa"
list["Jer"]="Jr Jer Jér"
list["Lam"]="Lm Lam"
list["Ezek"]="Ez Éz Eze Ézé Ezé"
list["Dan"]="Dn Dan"
list["Hos"]="Os"
list["Joel"]="Jl Joe Joel Joël"
list["Amos"]="Am Amos"
list["Obad"]="Ab Abd"
list["Jonah"]="Jon"
list["Mic"]="Mi Mic Mich"
list["Nah"]="Na Nah"
list["Hab"]="Ha Hab"
list["Zeph"]="So Sop Soph"
list["Hag"]="Ag Agg"
list["Zech"]="Za Zac Zach"
list["Mal"]="Ma Ml Mal"

list["Matt"]="Mt Mat Matthieu"
list["Mark"]="Mr Mar Marc"
list["Luke"]="Lu Lc Luc"
list["John"]="Jn Jean"
list["Acts"]="Ac Act Actes"
list["Rom"]="Rm Rom"
list["1Cor"]="1Co 1Cor"
list["2Cor"]="2Co 2Cor"
list["Gal"]="Ga Gal"
list["Eph"]="Ep Ép Eph Éph"
list["Phil"]="Phi Php Phil"
list["Col"]="Col"
list["1Thess"]="1Th 1Thes 1Thess"
list["2Thess"]="2Th 2Thes 2Thess"
list["1Tim"]="1Ti 1Tim"
list["2Tim"]="2Ti 2Tim"
list["Titus"]="Tit Tite"
list["Phlm"]="Phm Phlm"
list["Heb"]="He Hé Heb Héb"
list["Jas"]="Ja Jas Jc"
list["1Pet"]="1Pi"
list["2Pet"]="2Pi"
list["1John"]="1Jn 1Jean"
list["2John"]="2Jn 2Jean"
list["3John"]="3Jn 3Jean"
list["Jude"]="Jud Jude"
list["Rev"]="Ap Apo Apoc Re Ré Rv Rev Rév"

list["Bar"]="Bar"
list["Bel"]="Be"
list["Jdt"]="Jdt"
list["1Macc"]="1Ma 1Mc 1Mac 1Macc"
list["2Macc"]="2Ma 2Mc 2Mac 2Macc"
list["3Macc"]="3Ma 3Mc 3Mac 3Macc"
list["4Macc"]="4Ma 4Mc 4Mac 4Macc"
list["Sir"]="Sir"
list["Tob"]="Tb Tob"
list["Wis"]="Sg Sag"
list["4Ezra"]="4Esd"
list["1En"]="Hen Hén"

# List of OSIS Names
osisList="Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Esth Job Ps Prov Eccl Song Isa Jer Lam Ezek Dan Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev Rev Bar Bel Jdt 1Macc 2Macc 3Macc 4Macc Sir Tob Wis 4Ezra 1En"


# With the 2 above lists, we generate 2 variables and 1 array:
#  osisNames: contains the list of Osis Names
#  srcBooks : contains list of abbreviations found in the incoming text
#  srcAbbrv[] : contains the Osis Name matching an abbreviation

# Divide osisList and store pieces in osisArray -> osisArray[1]="Gen" and so on
split(osisList, osisArray)
for (canon in osisArray) {
   # Generating osisNames
   osisNames = osisNames "^" osisArray[canon] "$|"
   # Divide abbreviations list for each book
   split(list[osisArray[canon]], abbrevList)
   for (nickname in abbrevList) {
     # Generating books
     srcBooks = srcBooks "^" abbrevList[nickname] "$|"
     # Generating srcAbbrv[]
     srcAbbrv[abbrevList[nickname]] = osisArray[canon]
   }
}
# Remove unwanted trailing |
osisNames = substr(osisNames, 1, length(osisNames) - 1)
srcBooks =  substr(srcBooks, 1, length(srcBooks) - 1)


# 2. Variables used for checking chapters and verses numbers vs KJV v11n
# Numbers of chapter per book
maxchap["Gen"]=50
maxchap["Exod"]=40
maxchap["Lev"]=27
maxchap["Num"]=36
maxchap["Deut"]=34
maxchap["Josh"]=24
maxchap["Judg"]=21
maxchap["Ruth"]=4
maxchap["1Sam"]=31
maxchap["2Sam"]=24
maxchap["1Kgs"]=22
maxchap["2Kgs"]=25
maxchap["1Chr"]=29
maxchap["2Chr"]=36
maxchap["Ezra"]=10
maxchap["Neh"]=13
maxchap["Esth"]=10
maxchap["Job"]=42
maxchap["Ps"]=150
maxchap["Prov"]=31
maxchap["Eccl"]=12
maxchap["Song"]=8
maxchap["Isa"]=66
maxchap["Jer"]=52
maxchap["Lam"]=5
maxchap["Ezek"]=48
maxchap["Dan"]=12
maxchap["Hos"]=14
maxchap["Joel"]=3
maxchap["Amos"]=9
maxchap["Obad"]=1
maxchap["Jonah"]=4
maxchap["Mic"]=7
maxchap["Nah"]=3
maxchap["Hab"]=3
maxchap["Zeph"]=3
maxchap["Hag"]=2
maxchap["Zech"]=14
maxchap["Mal"]=4

maxchap["Matt"]=28
maxchap["Mark"]=16
maxchap["Luke"]=24
maxchap["John"]=21
maxchap["Acts"]=28
maxchap["Rom"]=16
maxchap["1Cor"]=16
maxchap["2Cor"]=13
maxchap["Gal"]=6
maxchap["Eph"]=6
maxchap["Phil"]=4
maxchap["Col"]=4
maxchap["1Thess"]=5
maxchap["2Thess"]=3
maxchap["1Tim"]=6
maxchap["2Tim"]=4
maxchap["Titus"]=3
maxchap["Phlm"]=1
maxchap["Heb"]=13
maxchap["Jas"]=5
maxchap["1Pet"]=5
maxchap["2Pet"]=3
maxchap["1John"]=5
maxchap["2John"]=1
maxchap["3John"]=1
maxchap["Jude"]=1
maxchap["Rev"]=22

maxchap["Tob"]=14
maxchap["Jdt"]=16
maxchap["Wis"]=19
maxchap["Sir"]=51
maxchap["Bar"]=6
maxchap["Bel"]=1
maxchap["1Macc"]=16
maxchap["2Macc"]=15
maxchap["3Macc"]=7
maxchap["4Macc"]=18
maxchap["4Ezra"]=16
maxchap["1En"]=105

# Numbers of verse per chapter from Ge to 1En
# Genesis
vm="31 25 24 26 32 22 24 22 29 32 32 20 18 24 21 16 27 33 38 18 34 24 20 67 34 35 46 22 35 43 55 32 20 31 29 43 36 30 23 23 57 38 34 34 28 34 31 22 33 26 "
# Exodus
vm=vm "22 25 22 31 23 30 25 32 35 29 10 51 22 31 27 36 16 27 25 26 36 31 33 18 40 37 21 43 46 38 18 35 23 35 35 38 29 31 43 38 "
# Leviticus
vm=vm "17 16 17 35 19 30 38 36 24 20 47 8 59 57 33 34 16 30 37 27 24 33 44 23 55 46 34 "
# Numbers
vm=vm "54 34 51 49 31 27 89 26 23 36 35 16 33 45 41 50 13 32 22 29 35 41 30 25 18 65 23 31 40 16 54 42 56 29 34 13 "
# Deuteronomy
vm=vm "46 37 29 49 33 25 26 20 29 22 32 32 18 29 23 22 20 22 21 20 23 30 25 22 19 19 26 68 29 20 30 52 29 12 "
# Joshua
vm=vm "18 24 17 24 15 27 26 35 27 43 23 24 33 15 63 10 18 28 51 9 45 34 16 33 "
# Judges
vm=vm "36 23 31 24 31 40 25 35 57 18 40 15 25 20 20 31 13 31 30 48 25 "
# Ruth
vm=vm "22 23 18 22 "
# I Samuel
vm=vm "28 36 21 22 12 21 17 22 27 27 15 25 23 52 35 23 58 30 24 42 15 23 29 22 44 25 12 25 11 31 13 "
# II Samuel
vm=vm "27 32 39 12 25 23 29 18 13 19 27 31 39 33 37 23 29 33 43 26 22 51 39 25 "
  // I Kings
vm=vm "53 46 28 34 18 38 51 66 28 29 43 33 34 31 34 34 24 46 21 43 29 53 "
# II Kings
vm=vm "18 25 27 44 27 33 20 29 37 36 21 21 25 29 38 20 41 37 37 21 26 20 37 20 30 "
# I Chronicles
vm=vm"  54 55 24 43 26 81 40 40 44 14  47 40 14 17 29 43 27 17 19 8  30 19 32 31 31 32 34 21 30 "
# II Chronicles
vm=vm"  17 18 17 22 14 42 22 18 31 19  23 16 22 15 19 14 19 34 11 37  20 12 21 27 28 23 9 27 36 27  21 33 25 33 27 23 "
# Ezra
vm=vm"  11 70 13 24 17 22 28 36 15 44 "
# Nehemiah
vm=vm"  11 20 32 23 19 19 73 18 38 39  36 47 31 "
# Esther
vm=vm"  22 23 15 17 14 14 10 17 32 3 "
# Job
vm=vm"  22 13 26 21 27 30 21 22 35 22  20 25 28 22 35 22 16 21 29 29  34 30 17 25 6 14 23 28 25 31  40 22 33 37 16 33 24 41 30 24  34 17 "
# Psalms
vm=vm"  6 12 8 8 12 10 17 9 20 18  7 8 6 7 5 11 15 50 14 9  13 31 6 10 22 12 14 9 11 12  24 11 22 22 28 12 40 22 13 17  13 11 5 26 17 11 9 14 20 23  19 9 6 7 23 13 11 11 17 12  8 12 11 10 13 20 7 35 36 5  24 20 28 23 10 12 20 72 13 19  16 8 18 12 13 17 7 18 52 17  16 15 5 23 11 13 12 9 9 5  8 28 22 35 45 48 43 13 31 7  10 10 9 8 18 19 2 29 176 7  8 9 4 8 5 6 5 6 8 8  3 18 3 3 21 26 9 8 24 13  10 7 12 15 21 10 20 14 9 6 "
# Proverbs
vm=vm"  33 22 35 27 23 35 27 36 18 32  31 28 25 35 33 33 28 24 29 30  31 29 35 34 28 28 27 28 27 33  31 "
# Ecclesiastes
vm=vm"  18 26 22 16 20 12 29 17 18 20  10 14 "
# Song of Solomon
vm=vm"  17 17 11 16 16 13 13 14 "
# Isaiah
vm=vm"  31 22 26 6 30 13 25 22 21 34  16 6 22 32 9 14 14 7 25 6  17 25 18 23 12 21 13 29 24 33  9 20 24 17 10 22 38 22 8 31  29 25 28 28 25 13 15 22 26 11  23 15 12 17 13 12 21 14 21 22  11 12 19 12 25 24 "
# Jeremiah
vm=vm"  19 37 25 31 31 30 34 22 26 25  23 17 27 22 21 21 27 23 15 18  14 30 40 10 38 24 22 17 32 24  40 44 26 22 19 32 21 28 18 16  18 22 13 30 5 28 7 47 39 46  64 34 "
# Lamentations
vm=vm"  22 22 66 22 22 "
# Ezekiel
vm=vm"  28 10 27 17 17 14 27 18 11 22  25 28 23 23 8 63 24 32 14 49  32 31 49 27 17 21 36 26 21 26  18 32 33 31 15 38 28 23 29 49  26 20 27 31 25 24 23 35 "
# Daniel
vm=vm"  21 49 30 37 31 28 28 27 27 21  45 13 "
# Hosea
vm=vm"  11 23 5 19 15 11 16 14 17 15  12 14 16 9 "
# Joel
vm=vm"  20 32 21 "
# Amos
vm=vm"  15 16 15 13 27 14 17 14 15 "
# Obadiah
vm=vm"  21 "
# Jonah
vm=vm"  17 10 10 11 "
# Micah
vm=vm"  16 13 12 13 15 16 20 "
# Nahum
vm=vm"  15 13 19 "
# Habakkuk
vm=vm"  17 20 19 "
# Zephaniah
vm=vm"  18 15 20 "
# Haggai
vm=vm"  15 23 "
# Zechariah
vm=vm"  21 13 10 14 11 15 14 23 17 12  17 14 9 21 "
# Malachi
vm=vm"  14 17 18 6 "
# - NT -
# Matthew
vm=vm"  25 23 17 25 48 34 29 34 38 42  30 50 58 36 39 28 27 35 30 34  46 46 39 51 46 75 66 20 "
# Mark
vm=vm"  45 28 35 41 43 56 37 38 50 52  33 44 37 72 47 20 "
# Luke
vm=vm"  80 52 38 44 39 49 50 56 62 42  54 59 35 35 32 31 37 43 48 47  38 71 56 53 "
# John
vm=vm"  51 25 36 54 47 71 53 59 41 42  57 50 38 31 27 33 26 40 42 31  25 "
# Acts
vm=vm"  26 47 26 37 42 15 60 40 43 48  30 25 52 28 41 40 34 28 41 38  40 30 35 27 27 32 44 31 "
# Romans
vm=vm"  32 29 31 25 21 23 25 39 33 21  36 21 14 23 33 27 "
# I Corinthians
vm=vm"  31 16 23 21 13 20 40 13 27 33  34 31 13 40 58 24 "
# II Corinthians
vm=vm"  24 17 18 18 21 18 16 24 15 18  33 21 14 "
# Galatians
vm=vm"  24 21 29 31 26 18 "
# Ephesians
vm=vm"  23 22 21 32 33 24 "
# Philippians
vm=vm"  30 30 21 23 "
# Colossians
vm=vm"  29 23 25 18 "
# I Thessalonians
vm=vm"  10 20 13 18 28 "
# II Thessalonians
vm=vm"  12 17 18 "
# I Timothy
vm=vm"  20 15 16 16 25 21 "
# II Timothy
vm=vm"  18 26 17 22 "
# Titus
vm=vm"  16 15 15 "
# Philemon
vm=vm"  25 "
# Hebrews
vm=vm"  14 18 19 16 14 20 28 13 28 39  40 29 25 "
# James
vm=vm"  27 26 18 17 20 "
# I Peter
vm=vm"  25 25 22 19 14 "
# II Peter
vm=vm"  21 22 18 "
# I John
vm=vm"  10 29 24 21 21 "
# II John
vm=vm"  13 "
# III John
vm=vm"  14 "
# Jude
vm=vm"  25 "
# Revelation of John
vm=vm"  20 29 22 11 14 17 17 13 21 11  19 17 18 20 8 21 18 24 21 15  27 21"

# Tobit
vm=vm"  25 23 25 23 28 22 20 24 12 13  21 22 23 17"
# Judith
vm=vm"  12 18 15 17 29 21 25 34 19 20  21 20 31 18 15 31"
# Wisdom
vm=vm"  16 25 19 20 23 25 30 21 18 21  26 27 19 31 19 29 21 25 22"
# Sirach
vm=vm"  30 18 29 31 15 37 36 19 18 30  32 18 25 27 20 28 27 32 27 30  28 25 27 32 25 20 30 26 28 25  31 23 31 26 26 28 31 34 35 30  22 25 33 23 26 20 25 24 16 29  30"
# Baruch
vm=vm"  22 35 38 37 9 72"
# Bel
vm=vm"  42"
# I Maccabees
vm=vm"  67 70 60 61 68 63 50 32 73 89  74 54 54 49 41 24"
# II Maccabees
vm=vm"  36 33 40 50 27 31 42 36 29 38  38 46 26 46 40"
# III Maccabees
vm=vm"  29 33 30 21 51 41 23"
# IV Maccabees
vm=vm"  35 24 21 26 38 35 23 29 32 21  27 19 27 20 32 25 24 24"
# 4 Ezra
vm=vm"  40 48 36 52 56 59 139 63 47 60  46 51 58 47 63 78"
# Enoch
vm=vm"  8 1 3 1 1 12 15 9 14 29  0 7 11 25 10 5 6 18 3 7  6 15 5 11 3 4 3 3 3 2  5 4 4 2 3 0 3 6 12 10  7 2 2 1 5 6 4 11 4 4 5  10 7 11 12 6 5 5 14 15 16  18 16 1 11 4 15 5 42 4 24  47 10 16 15 13 8 21 5 10 13  25 12 8 15 8 3 5 118 51 17  3 24 11 6 8 25 16 3 10 9  7 14 11 2 27"

# With the last 2 arrays maxchap[] and vm[], we generate 2 other arrays:
#  vmax[]     : contains number of verses per chapter, for all books from Ge to 1En
#  startchap[]: hold the index in vmax corresponding to a given chapter of a book
#
# The idea is to return the number of verses for a given book and chapter
# example: vmax[startchap["Heb"]+1] returns the number of verses of Hebrew 1
# -> vmax[startchap["Heb"]+1] = 14

# Generating vmax
split(vm,vmax," ")

# Generating startchap
split("Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Esth Job Ps Prov Eccl Song Isa Jer Lam Ezek Dan Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev Tob Jdt Wis Sir Bar Bel 1Macc 2Macc 3Macc 4Macc 4Ezra 1En",roll," ")
sumchap=0
for (i in roll) {
  startchap[roll[i]]=sumchap+maxchap[roll[i-1]]
  sumchap=startchap[roll[i]]
}
##print "Ref=" vmax[startchap["Heb"]+1] > "/dev/stderr" # for debugging

# 3. Other Variables
# separators encountered between words
sep="[ |\\-|\\.|,|:|;|\\(|\\)|<|>|=|\\?]"

# templates for SWORD reference tags
t_plain="<reference osisRef=\"%s.%s.%s\">%s"
t_range="<reference osisRef=\"%s.%s.%s-%s.%s.%s\">%s"
t_short="<reference osisRef=\"%s.%s.%s\">%s</reference>"
t_close="%s</reference>"
if (modtype == "TEI") {
t_plain="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s\">%s"
t_range="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s-%s.%s.%s\">%s"
t_short="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s\">%s</ref></xr>"
t_close="%s</ref></xr>"
}

# error management
stop="F"
warn="W"
info="I"
# fatal error=1 -> process stopped
fatal=0
# error=1 -> display error
error=0

}




#
# Functions
#

# Function to validate a reference
function validref(book,chapter,verse)
{
 REFOK=match(book,osisNames)
 if (REFOK) {
  chapter=chapter+0
  verse=verse+0
  if (chapter < 1) REFOK=0
  else {
   if (verse < 1) REFOK=0
   else {
    if (chapter > maxchap[book]){
      print book, chapter " > " book, maxchap[book] " max!" > "/dev/stderr"
     REFOK=0
    }
    else {
     if (verse > vmax[startchap[book]+chapter]) {
      print book, chapter ":" verse " > "  book, chapter ":" vmax[startchap[book]+chapter] " max!" > "/dev/stderr"
      REFOK=0
     }
    }
   }
  }
 }
 else printf "«%s» is not a valid OSIS name\n", book > "/dev/stderr"
 return REFOK
}

# Function to save errors encoutered during process
function save_error(gravity,type,start,end)
{
 # gravity  : stop, warn, info
 # type     : no_chapter  : book alone, no chapter found
 #            full_chapter: book+chapter only found, no verses
 #            no_verse    : book, chapter and separator found, no verses
 #            isolated    : number found without preceding book
 #            lower_verse : verse lower than previous one
 #            integrity   : modified text different than original

 # Add information do debug msg
 # start, end: indexes of dbg_info[]
 dbg_info[start]=sprintf("[%s-%s:%s",gravity, type, dbg_info[start])
 dbg_info[end]=sprintf("%s]", dbg_info[end])
 # allow error to be displayed
 error++
 # fatal error
 if (gravity == stop) {
  fatal++
  display_fatal++
 }
}

#
# Main
#

{
 # 1. Scan a line and divise string into words
 # 1.1 Reset variables
 len=length($0)
 mode="text"
 nblocks=1
 word[nblocks]=""
 dbg_info[nblocks]=""
 last_block=0
 end_verse="0"
 error=0
 display_fatal=0
 # 1.2 Scan line
 for (i=1; i<=len; i++) {
  # Extract char
  char=substr($0, i, 1)
  # Search for a separator
  if (match(char,sep))
   new_mode="separator"
  else
   new_mode="text"
  # Increment n
  if (new_mode != mode){
   mode=new_mode
   nblocks++
   word[nblocks]=""
   dbg_info[nblocks]=""
  }
  # Fill in data
  word[nblocks]=word[nblocks]char
  dbg_info[nblocks]=dbg_info[nblocks]char
 }
 # 2. Search for a biblical reference
 for ( i=1 ; i<=nblocks ; i++ ) {
  # 2.1 Full reference
  # Search for a book name
  if (match(word[i],srcBooks)) {
   # Book name found
   book=srcAbbrv[word[i]]
   # Search for a chapter number
   if (i+2 <= nblocks) {
    if (match(word[i+2],/^[0-9]{1,3}$/)) {
     # Chapter number found
     stt_chapter=word[i+2]
     end_chapter=stt_chapter
     last_block=i+2
     # Search for a verse number
     if (i+4 <= nblocks) {
	 if (match(word[i+3], ":|,")) {
       if (match(word[i+4],/^[0-9]{1,3}$/)) {
        # Verse number found (ex: Ge 12:4)
        stt_verse=word[i+4]
        end_verse=stt_verse
        last_block=i+4
        UNIQ=1
        # Search for a verses range
        if (i+6 <= nblocks) {
         if (word[i+5] == "-") {
          if (match(word[i+6],/^[0-9]{1,3}$/)) {
           # Range verse found (ex: Ge 12:4-5)
           end_verse=word[i+6]
           last_block=i+6
           # Search for an extended range (ex: Ge 12:17-13:1)
           if (i+8 <= nblocks) {
	       if (match(word[i+7], ":|,")) {
             if (match(word[i+8],/^[0-9]{1,3}$/)) {
              # Extended range found
              end_chapter=word[i+6]
              end_verse=word[i+8]
              last_block=i+8
             }
             else save_error(info,"no_verse",i,last_block)
            } # Continue: Extended range
           } # Continue: End of line found
            UNIQ=0
          } # Continue: No Range verse
          else save_error(info,"no_verse",i,last_block)
         } # Continue: No Range verse
        } # Continue: End of line found
        # Insert osisRef
        if (UNIQ) {
         # one reference
         if (validref(book,stt_chapter,stt_verse)) {
          word[i]=sprintf(t_plain,book,end_chapter,stt_verse,word[i])
          word[last_block]=sprintf(t_close,word[last_block])
         }
         else save_error(info,"not_in_v11n",i,last_block)
        }
        else {
         # range of references
         if (validref(book,stt_chapter,stt_verse) && validref(book,end_chapter,end_verse)) {
          if (end_chapter - stt_chapter < 2) {
           word[i]=sprintf(t_range,book,stt_chapter,stt_verse,book,end_chapter,end_verse,word[i])
           word[last_block]=sprintf(t_close,word[last_block])
          }
         }
         else save_error(info,"not_in_v11n",i,last_block)
        }
       }
       else save_error(info,"no_verse",i,last_block)
      }
      else {
       last_block++
       save_error(info,"full_chapter",i,last_block)
      }
     } # Continue: End of line found
     else save_error(info,"no_chapter",i,i+2)
    }
   } # Continue: End of line found
  } # Continue: No book found

  # 2.2 Partial references following a full reference
  # Search for a single number (ex: 22)
  if (i == last_block+2) {
   if (match(word[i],/^[0-9]{1,3}$/)) {
    # Simple verse found
    stt_chapter=end_chapter
    stt_verse=word[i]
    last_block=i
    UNIQ=1
    # Search for range of verses (ex: 25-28)
    if (+2 <= nblocks) {
     if (word[i+1] == "-") {
      if (match(word[i+2],/^[0-9]{1,3}$/)) {
       # Range of verses found
       end_verse=word[i+2]
       last_block=i+2
       UNIQ=0
      }
      else save_error(info,"no_verse",i,last_block)
     } # Continue: No Range of verses
     # Search for a reference with chapter (ex: 23:19)
     if (match(word[i+1], ":|,")) {
      if (word[i+2]+0 > 0) {
       # Reference with chapter found
       stt_chapter=word[i]
       end_chapter=stt_chapter
       stt_verse=word[i+2]
       last_block=i+2
       UNIQ=1
       # Search for a reference with chapter and range of verses (ex: 23:19-21)
       if (i+4 <= nblocks) {
        if (word[i+3] == "-") {
         if (match(word[i+4],/^[0-9]{1,3}$/)) {
          # Reference with chapter and range of verses found
          end_verse=word[i+4]
          last_block=i+4
          UNIQ=0
         }
         else save_error(info,"no_verse",i,last_block)
        } # Continue: No reference with chapter and range of verses
       } # Continue: End of line found
      }
      else save_error(info,"no_verse",i,last_block)
     } # Continue: No reference with chapter
     # Insert osisRef
     if (UNIQ) {
      # unique reference
      if (validref(book,stt_chapter,stt_verse)) {
       word[i]=sprintf(t_plain,book,end_chapter,stt_verse,word[i])
       word[last_block]=sprintf(t_close,word[last_block])
      }
      else save_error(info,"not_in_v11n",i,last_block)
     }
     else {
      # range of references
      if (validref(book,stt_chapter,stt_verse) && validref(book,end_chapter,end_verse)) {
       if (end_chapter - stt_chapter < 2) {
        word[i]=sprintf(t_range,book,stt_chapter,stt_verse,book,end_chapter,end_verse,word[i])
        word[last_block]=sprintf(t_close,word[last_block])
       }
      }
      else save_error(info,"not_in_v11n",i,last_block)
     } # End of insert
    } # Continue: End of line found
   } # Continue: Word[i] is not a number
  } # Continue: End of line found
  else {
   if (match(word[i],/^[0-9]{1,3}$/)) {
    if (i > last_block+2) {
     # Alone number, max verse number is 176 - Psalm 119
     if ((word[i]+0 <= 176) && (! match(word[i-2],srcBooks))) {
      if ((word[i-1] == ":") || (word[i+1] == ":"))
       level=warn
      else level=info
       save_error(level,"isolated",i,i)
     }
    }
   }
  }
 }

 # 3. Check integrity
 # 3.1 Rebuild the full string
 check=""
 for ( i=1 ; i<=nblocks ; i++ ) {
  check=check""word[i]
 }

 # 3.2 Remove added SWORD links
 if (modtype == "TEI") {
  gsub(/<xr type=.Bible.>/,"",check)
  gsub(/<ref osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<ref osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}-[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<\/ref><\/xr>/,"",check)
 }
 else {
  gsub(/<reference osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<reference osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}-[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<\/reference>/,"",check)
 }

 # 3.3 Check result against the initial value ($0)
 if (check != $0) {save_error(stop,"integrity",1,n)}

 # 4. Print
 # 4.1 Regular ouptput
 for ( i=1 ; i<=nblocks ; i++ ) {
  printf "%s", word[i]
 }
 printf "\n"

 # 4.2 Debugging information
 if (error) {
  if (debug) {
   printf "Line=%s:\n",NR  > "/dev/stderr"
   for ( i=1 ; i<=nblocks ; i++ ) {
    printf "%s", dbg_info[i]  > "/dev/stderr"
   }
   printf "\n"  > "/dev/stderr"
  }
  if (display_fatal) {
   printf "[  -original:%s]\n", $0 > "/dev/stderr"
   printf "[  -computed:%s]\n", check > "/dev/stderr"
  }
 }
}

END{
 # Quit on fatal errors
 if (fatal) {exit 1}
}


