#!/bin/awk -f

#
## verse_link.awk - dominique@corbex.org - 2019
#
# This script will insert valid OSIS references to a given file
#
# options
#   modtype : TEI or OSIS (osisRef formatting differs)
#   v11n    : Versification (default: KJV, options are 'Catholic' or 'Calvin')
#   debug   : > 0 to see error msgs printed to /dev/stderr

#
# Script history
#   1.0 Initial release

BEGIN{
## 1. Variables used for matching abbreviations with Osis names
# list of abbreviations
list["Gen"]="Ge Gn Gen"
list["Exod"]="Ex Exo Exod"
list["Lev"]="Le Lé Lv Lev Lév"
list["Num"]="No Nb Nom"
list["Deut"]="De Dt Deu Deut"
list["Josh"]="Jos"
list["Judg"]="Jg Jug"
list["Ruth"]="Ru Rut Ruth"
list["1Sam"]="1S 1Sa 1Sam"
list["2Sam"]="2S 2Sa 2Sam"
list["1Kgs"]="1R 1Ro 1Rois"
list["2Kgs"]="2R 2Ro 2Rois"
list["1Chr"]="1Ch 1Chr"
list["2Chr"]="2Ch 2Chr"
list["Ezra"]="Esd"
list["Neh"]="Ne Neh Né Néh"
list["Esth"]="Est Esth"
list["Job"]="Jb Job"
list["Ps"]="Ps Psa"
list["Prov"]="Pr Pro"
list["Eccl"]="Ec Ecc Eccl"
list["Song"]="Ca Ct"
list["Isa"]="Es És Esa Ésa Is Isa"
list["Jer"]="Jr Jer Jér"
list["Lam"]="Lm Lam"
list["Ezek"]="Ez Éz Eze Ézé Ezé"
list["Dan"]="Dn Dan"
list["Hos"]="Os"
list["Joel"]="Jl Joe Joel Joël"
list["Amos"]="Am Amos"
list["Obad"]="Ab Abd"
list["Jonah"]="Jon"
list["Mic"]="Mi Mic Mich"
list["Nah"]="Na Nah"
list["Hab"]="Ha Hb Hab"
list["Zeph"]="So Sop Soph"
list["Hag"]="Ag Agg"
list["Zech"]="Za Zac Zach"
list["Mal"]="Ma Ml Mal"

list["Matt"]="Mt Mat Matthieu"
list["Mark"]="Mr Mar Marc Mc"
list["Luke"]="Lu Lc Luc"
list["John"]="Jn Jean"
list["Acts"]="Ac Act Actes"
list["Rom"]="Rm Rom"
list["1Cor"]="1Co 1Cor"
list["2Cor"]="2Co 2Cor"
list["Gal"]="Ga Gal"
list["Eph"]="Ep Ép Eph Éph"
list["Phil"]="Phi Php Phil Ph"
list["Col"]="Col"
list["1Thess"]="1Th 1Thes 1Thess"
list["2Thess"]="2Th 2Thes 2Thess"
list["1Tim"]="1Ti 1Tim"
list["2Tim"]="2Ti 2Tim"
list["Titus"]="Tit Tite Tt"
list["Phlm"]="Phm Phlm"
list["Heb"]="He Hé Heb Héb"
list["Jas"]="Ja Jas Jc"
list["1Pet"]="1Pi"
list["2Pet"]="2Pi"
list["1John"]="1Jn 1Jean"
list["2John"]="2Jn 2Jean"
list["3John"]="3Jn 3Jean"
list["Jude"]="Jud Jude"
list["Rev"]="Ap Apo Apoc Re Ré Rv Rev Rév"

list["Bar"]="Ba Bar"
list["Bel"]="Be Bel"
list["Jdt"]="Jdt"
list["1Macc"]="1Ma 1Mc 1Mac 1Macc"
list["2Macc"]="2Ma 2Mc 2Mac 2Macc"
list["3Macc"]="3Ma 3Mc 3Mac 3Macc"
list["4Macc"]="4Ma 4Mc 4Mac 4Macc"
list["Sir"]="Sir"
list["Tob"]="Tb Tob"
list["Wis"]="Sg Sag"
list["4Ezra"]="4Esd"
list["1En"]="Hen Hén"

# 2. Variables used for checking chapters and verses numbers
# Default is KJV v11n

# KJV Canon
#
canon="Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Esth Job Ps Prov Eccl Song Isa Jer Lam Ezek Dan Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev"

# Numbers of chapters per book
# OT
maxchap["Gen"]=50
maxchap["Exod"]=40
maxchap["Lev"]=27
maxchap["Num"]=36
maxchap["Deut"]=34
maxchap["Josh"]=24
maxchap["Judg"]=21
maxchap["Ruth"]=4
maxchap["1Sam"]=31
maxchap["2Sam"]=24
maxchap["1Kgs"]=22
maxchap["2Kgs"]=25
maxchap["1Chr"]=29
maxchap["2Chr"]=36
maxchap["Ezra"]=10
maxchap["Neh"]=13
maxchap["Esth"]=10
maxchap["Job"]=42
maxchap["Ps"]=150
maxchap["Prov"]=31
maxchap["Eccl"]=12
maxchap["Song"]=8
maxchap["Isa"]=66
maxchap["Jer"]=52
maxchap["Lam"]=5
maxchap["Ezek"]=48
maxchap["Dan"]=12
maxchap["Hos"]=14
maxchap["Joel"]=3
maxchap["Amos"]=9
maxchap["Obad"]=1
maxchap["Jonah"]=4
maxchap["Mic"]=7
maxchap["Nah"]=3
maxchap["Hab"]=3
maxchap["Zeph"]=3
maxchap["Hag"]=2
maxchap["Zech"]=14
maxchap["Mal"]=4

# NT
maxchap["Matt"]=28
maxchap["Mark"]=16
maxchap["Luke"]=24
maxchap["John"]=21
maxchap["Acts"]=28
maxchap["Rom"]=16
maxchap["1Cor"]=16
maxchap["2Cor"]=13
maxchap["Gal"]=6
maxchap["Eph"]=6
maxchap["Phil"]=4
maxchap["Col"]=4
maxchap["1Thess"]=5
maxchap["2Thess"]=3
maxchap["1Tim"]=6
maxchap["2Tim"]=4
maxchap["Titus"]=3
maxchap["Phlm"]=1
maxchap["Heb"]=13
maxchap["Jas"]=5
maxchap["1Pet"]=5
maxchap["2Pet"]=3
maxchap["1John"]=5
maxchap["2John"]=1
maxchap["3John"]=1
maxchap["Jude"]=1
maxchap["Rev"]=22

# Deuterocanonical books
maxchap["Tob"]=14
maxchap["Jdt"]=16
maxchap["Wis"]=19
maxchap["Sir"]=51
maxchap["Bar"]=6
maxchap["Bel"]=1
maxchap["1Macc"]=16
maxchap["2Macc"]=15
maxchap["3Macc"]=7
maxchap["4Macc"]=18
maxchap["4Ezra"]=16
maxchap["1En"]=105


# Numbers of verse per chapter
# Genesis
vm_kjv["Gen"]="31 25 24 26 32 22 24 22 29 32 32 20 18 24 21 16 27 33 38 18 34 24 20 67 34 35 46 22 35 43 55 32 20 31 29 43 36 30 23 23 57 38 34 34 28 34 31 22 33 26"
# Exodus
vm_kjv["Exod"]="22 25 22 31 23 30 25 32 35 29 10 51 22 31 27 36 16 27 25 26 36 31 33 18 40 37 21 43 46 38 18 35 23 35 35 38 29 31 43 38"
# Leviticus
vm_kjv["Lev"]="17 16 17 35 19 30 38 36 24 20 47 8 59 57 33 34 16 30 37 27 24 33 44 23 55 46 34"
# Numbers
vm_kjv["Num"]="54 34 51 49 31 27 89 26 23 36 35 16 33 45 41 50 13 32 22 29 35 41 30 25 18 65 23 31 40 16 54 42 56 29 34 13"
# Deuteronomy
vm_kjv["Deut"]="46 37 29 49 33 25 26 20 29 22 32 32 18 29 23 22 20 22 21 20 23 30 25 22 19 19 26 68 29 20 30 52 29 12"
# Joshua
vm_kjv["Josh"]="18 24 17 24 15 27 26 35 27 43 23 24 33 15 63 10 18 28 51 9 45 34 16 33"
# Judges
vm_kjv["Judg"]="36 23 31 24 31 40 25 35 57 18 40 15 25 20 20 31 13 31 30 48 25"
# Ruth
vm_kjv["Ruth"]="22 23 18 22"
# I Samuel
vm_kjv["1Sam"]="28 36 21 22 12 21 17 22 27 27 15 25 23 52 35 23 58 30 24 42 15 23 29 22 44 25 12 25 11 31 13"
# II Samuel
vm_kjv["2Sam"]="27 32 39 12 25 23 29 18 13 19 27 31 39 33 37 23 29 33 43 26 22 51 39 25"
  // I Kings
vm_kjv["1Kgs"]="53 46 28 34 18 38 51 66 28 29 43 33 34 31 34 34 24 46 21 43 29 53"
# II Kings
vm_kjv["2Kgs"]="18 25 27 44 27 33 20 29 37 36 21 21 25 29 38 20 41 37 37 21 26 20 37 20 30"
# I Chronicles
vm_kjv["1Chr"]="54 55 24 43 26 81 40 40 44 14  47 40 14 17 29 43 27 17 19 8  30 19 32 31 31 32 34 21 30"
# II Chronicles
vm_kjv["2Chr"]="17 18 17 22 14 42 22 18 31 19  23 16 22 15 19 14 19 34 11 37  20 12 21 27 28 23 9 27 36 27  21 33 25 33 27 23"
# Ezra
vm_kjv["Ezra"]="11 70 13 24 17 22 28 36 15 44"
# Nehemiah
vm_kjv["Neh"]="11 20 32 23 19 19 73 18 38 39  36 47 31"
# Esther
vm_kjv["Esth"]="22 23 15 17 14 14 10 17 32 3"
# Job
vm_kjv["Job"]="22 13 26 21 27 30 21 22 35 22  20 25 28 22 35 22 16 21 29 29  34 30 17 25 6 14 23 28 25 31  40 22 33 37 16 33 24 41 30 24  34 17"
# Psalms
vm_kjv["Ps"]="6 12 8 8 12 10 17 9 20 18  7 8 6 7 5 11 15 50 14 9  13 31 6 10 22 12 14 9 11 12  24 11 22 22 28 12 40 22 13 17  13 11 5 26 17 11 9 14 20 23  19 9 6 7 23 13 11 11 17 12  8 12 11 10 13 20 7 35 36 5  24 20 28 23 10 12 20 72 13 19  16 8 18 12 13 17 7 18 52 17  16 15 5 23 11 13 12 9 9 5  8 28 22 35 45 48 43 13 31 7  10 10 9 8 18 19 2 29 176 7  8 9 4 8 5 6 5 6 8 8  3 18 3 3 21 26 9 8 24 13  10 7 12 15 21 10 20 14 9 6"
# Proverbs
vm_kjv["Prov"]="33 22 35 27 23 35 27 36 18 32  31 28 25 35 33 33 28 24 29 30  31 29 35 34 28 28 27 28 27 33  31"
# Ecclesiastes
vm_kjv["Eccl"]="18 26 22 16 20 12 29 17 18 20  10 14"
# Song of Solomon
vm_kjv["Song"]="17 17 11 16 16 13 13 14"
# Isaiah
vm_kjv["Isa"]="31 22 26 6 30 13 25 22 21 34  16 6 22 32 9 14 14 7 25 6  17 25 18 23 12 21 13 29 24 33  9 20 24 17 10 22 38 22 8 31  29 25 28 28 25 13 15 22 26 11  23 15 12 17 13 12 21 14 21 22  11 12 19 12 25 24"
# Jeremiah
vm_kjv["Jer"]="19 37 25 31 31 30 34 22 26 25  23 17 27 22 21 21 27 23 15 18  14 30 40 10 38 24 22 17 32 24  40 44 26 22 19 32 21 28 18 16  18 22 13 30 5 28 7 47 39 46  64 34"
# Lamentations
vm_kjv["Lam"]="22 22 66 22 22"
# Ezekiel
vm_kjv["Ezek"]="28 10 27 17 17 14 27 18 11 22  25 28 23 23 8 63 24 32 14 49  32 31 49 27 17 21 36 26 21 26  18 32 33 31 15 38 28 23 29 49  26 20 27 31 25 24 23 35"
# Daniel
vm_kjv["Dan"]="21 49 30 37 31 28 28 27 27 21  45 13"
# Hosea
vm_kjv["Hos"]="11 23 5 19 15 11 16 14 17 15  12 14 16 9"
# Joel
vm_kjv["Joel"]="20 32 21"
# Amos
vm_kjv["Amos"]="15 16 15 13 27 14 17 14 15"
# Obadiah
vm_kjv["Obad"]="21"
# Jonah
vm_kjv["Jonah"]="17 10 10 11"
# Micah
vm_kjv["Mic"]="16 13 12 13 15 16 20"
# Nahum
vm_kjv["Nah"]="15 13 19"
# Habakkuk
vm_kjv["Hab"]="17 20 19"
# Zephaniah
vm_kjv["Zeph"]="18 15 20"
# Haggai
vm_kjv["Hag"]="15 23"
# Zechariah
vm_kjv["Zech"]="21 13 10 14 11 15 14 23 17 12  17 14 9 21"
# Malachi
vm_kjv["Mal"]="14 17 18 6"
# - NT -
# Matthew
vm_kjv["Matt"]="25 23 17 25 48 34 29 34 38 42  30 50 58 36 39 28 27 35 30 34  46 46 39 51 46 75 66 20"
# Mark
vm_kjv["Mark"]="45 28 35 41 43 56 37 38 50 52  33 44 37 72 47 20"
# Luke
vm_kjv["Luke"]="80 52 38 44 39 49 50 56 62 42  54 59 35 35 32 31 37 43 48 47  38 71 56 53"
# John
vm_kjv["John"]="51 25 36 54 47 71 53 59 41 42  57 50 38 31 27 33 26 40 42 31  25"
# Acts
vm_kjv["Acts"]="26 47 26 37 42 15 60 40 43 48  30 25 52 28 41 40 34 28 41 38  40 30 35 27 27 32 44 31"
# Romans
vm_kjv["Rom"]="32 29 31 25 21 23 25 39 33 21  36 21 14 23 33 27"
# I Corinthians
vm_kjv["1Cor"]="31 16 23 21 13 20 40 13 27 33  34 31 13 40 58 24"
# II Corinthians
vm_kjv["2Cor"]="24 17 18 18 21 18 16 24 15 18  33 21 14"
# Galatians
vm_kjv["Gal"]="24 21 29 31 26 18"
# Ephesians
vm_kjv["Eph"]="23 22 21 32 33 24"
# Philippians
vm_kjv["Phil"]="30 30 21 23"
# Colossians
vm_kjv["Col"]="29 23 25 18"
# I Thessalonians
vm_kjv["1Thess"]="10 20 13 18 28"
# II Thessalonians
vm_kjv["2Thess"]="12 17 18"
# I Timothy
vm_kjv["1Tim"]="20 15 16 16 25 21"
# II Timothy
vm_kjv["2Tim"]="18 26 17 22"
# Titus
vm_kjv["Titus"]="16 15 15"
# Philemon
vm_kjv["Phlm"]="25"
# Hebrews
vm_kjv["Heb"]="14 18 19 16 14 20 28 13 28 39  40 29 25"
# James
vm_kjv["Jas"]="27 26 18 17 20"
# I Peter
vm_kjv["1Pet"]="25 25 22 19 14"
# II Peter
vm_kjv["2Pet"]="21 22 18"
# I John
vm_kjv["1John"]="10 29 24 21 21"
# II John
vm_kjv["2John"]="13"
# III John
vm_kjv["3John"]="14"
# Jude
vm_kjv["Jude"]="25"
# Revelation of John
vm_kjv["Rev"]="20 29 22 11 14 17 17 13 21 11  19 17 18 20 8 21 18 24 21 15  27 21"


# Build vm (verses max per chapter)
vm = ""
split(canon, canon_array)
for (canonic_book in canon_array) {
    vm = vm " " vm_kjv[canon_array[canonic_book]]
}


if (match(v11N, /[Cc]alvin/)) {
 # Calvin v11n
 # Canon is unchanged

 # Numbers of chapters per book - unchanged

 # Numbers of verse per chapter
 # Genesis
 vm_calvin["Gen"]="31 25 24 26 32 22 24 22 29 32  32 20 18 24 21 16 27 33 38 18  34 24 20 67 34 35 46 22 35 43  55 32 20 31 29 43 36 30 23 23  57 38 34 34 28 34 31 22 33 26"
 # Exodus
 vm_calvin["Exod"]="22 25 22 31 23 30 25 32 35 29  10 51 22 31 27 36 16 27 25 26  36 31 33 18 40 37 21 43 46 38  18 35 23 35 35 38 29 31 43 38"
 # Leviticus
 vm_calvin["Lev"]="17 16 17 35 19 30 38 36 24 20  47 8 59 57 33 34 16 30 37 27  24 33 44 23 55 46 34"
 # Numbers
 vm_calvin["Num"]="54 34 51 49 31 27 89 26 23 36  35 15 34 45 41 50 13 32 22 29  35 41 30 25 18 65 23 31 39 17  54 42 56 29 34 13"
 # Deuteronomy
 vm_calvin["Deut"]="46 37 29 49 33 25 26 20 29 22  32 32 18 29 23 22 20 22 21 20  23 30 25 22 19 19 26 68 29 20  30 52 29 12"
 # Joshua
 vm_calvin["Josh"]="18 24 17 24 15 27 26 35 27 43  23 24 33 15 63 10 18 28 51 9  45 34 16 33"
 # Judges
 vm_calvin["Judg"]="36 23 31 24 31 40 25 35 57 18  40 15 25 20 20 31 13 31 30 48  25"
 # Ruth
 vm_calvin["Ruth"]="22 23 18 22"
 # I Samuel
 vm_calvin["1Sam"]="28 36 21 22 12 21 17 22 27 27  15 25 23 52 35 23 58 30 24 43  15 23 28 23 44 25 12 25 11 31  13"
 # II Samuel
 vm_calvin["2Sam"]="27 32 39 12 25 23 29 18 13 19  27 31 39 33 37 23 29 33 43 26  22 51 39 25"
 # I Kings
 vm_calvin["1Kgs"]="53 46 28 34 18 38 51 66 28 29  43 33 34 31 34 34 24 46 21 43  29 54"
 # II Kings
 vm_calvin["2Kgs"]="18 25 27 44 27 33 20 29 37 36  21 21 25 29 38 20 41 37 37 21  26 20 37 20 30"
 # I Chronicles
 vm_calvin["1Chr"]="54 55 24 43 26 81 40 40 44 14  47 40 14 17 29 43 27 17 19 8  30 19 32 31 31 32 34 21 30"
 # II Chronicles
 vm_calvin["2Chr"]="17 18 17 22 14 42 22 18 31 19  23 16 22 15 19 14 19 34 11 37  20 12 21 27 28 23 9 27 36 27  21 33 25 33 27 23"
 # Ezra
 vm_calvin["Ezra"]="11 70 13 24 17 22 28 36 15 44"
 # Nehemiah
 vm_calvin["Neh"]="11 20 32 23 19 19 73 18 38 39  36 47 31"
 # Esther
 vm_calvin["Esth"]="22 23 15 17 14 14 10 17 32 3"
 # Job
 vm_calvin["Job"]="22 13 26 21 27 30 21 22 35 22  20 25 28 22 35 22 16 21 29 29  34 30 17 25 6 14 23 28 25 31  40 22 33 37 16 33 24 38 38 28  25 17"
 # Psalms
 vm_calvin["Ps"]="6 12 9 9 13 11 18 10 21 18  7 9 6 7 5 11 15 51 15 10  14 32 6 10 22 12 14 9 11 13  25 11 22 23 28 13 40 23 14 18  14 12 5 27 18 12 10 15 21 23  21 11 7 9 24 14 12 12 18 14  9 13 12 11 14 20 8 36 37 6  24 20 28 23 11 13 21 72 13 20  17 8 19 13 14 17 7 19 53 17  16 16 5 23 11 13 12 9 9 5  8 29 22 35 45 48 43 14 31 7  10 10 9 8 18 19 2 29 176 7  8 9 4 8 5 6 5 6 8 8  3 18 3 3 21 26 9 8 24 14  10 8 12 15 21 10 20 14 9 6"
 # Proverbs
 vm_calvin["Prov"]="33 22 35 27 23 35 27 36 18 32  31 28 25 35 33 33 28 24 29 30  31 29 35 34 28 28 27 28 27 33  31"
 # Ecclesiastes
 vm_calvin["Eccl"]="18 26 22 16 20 12 29 17 18 20  8 16"
 # Song of Solomon
 vm_calvin["Song"]="17 17 11 16 16 13 13 14"
 # Isaiah
 vm_calvin["Isa"]="31 22 26 6 30 13 25 23 20 34  16 6 22 32 9 14 14 7 25 6  17 25 18 23 12 21 13 29 24 33  9 20 24 17 10 22 38 22 8 31  29 25 28 28 25 13 15 22 26 11  23 15 12 17 13 12 21 14 21 22  11 12 19 12 25 24"
 # Jeremiah
 vm_calvin["Jer"]="19 37 25 31 31 30 34 22 26 25  23 17 27 22 21 21 27 23 15 18  14 30 40 10 38 24 22 17 32 24  40 44 26 22 19 32 21 28 18 16  18 22 13 30 5 28 7 47 39 46  64 34"
 # Lamentations
 vm_calvin["Lam"]="22 22 66 22 22"
 # Ezekiel
 vm_calvin["Ezek"]="28 10 27 17 17 14 27 18 11 22  25 28 23 23 8 63 24 32 14 44  37 31 49 27 17 21 36 26 21 26  18 32 33 31 15 38 28 23 29 49  26 20 27 31 25 24 23 35"
 # Daniel
 vm_calvin["Dan"]="21 49 30 37 31 28 28 27 27 21  45 13"
 # Hosea
 vm_calvin["Hos"]="11 23 5 19 15 11 16 14 17 15  11 15 16 9"
 # Joel
 vm_calvin["Joel"]="20 32 21"
 # Amos
 vm_calvin["Amos"]="15 16 15 13 27 14 17 14 15"
 # Obadiah
  vm_calvin["Obad"]=" 21"
 # Jonah
 vm_calvin["Jonah"]="16 11 10 11"
 # Micah
 vm_calvin["Mic"]="16 13 12 13 15 16 20"
 # Nahum
 vm_calvin["Nah"]="15 13 19"
 # Habakkuk
 vm_calvin["Hab"]="17 20 19"
 # Zephaniah
 vm_calvin["Zeph"]="18 15 20"
 # Haggai
 vm_calvin["Hag"]="15 23"
 # Zechariah
 vm_calvin["Zech"]="21 13 10 14 11 15 14 23 17 12  17 14 9 21"
 # Malachi
 vm_calvin["Mal"]="14 17 18 6"

 # NT
 # Matthew
 vm_calvin["Matt"]="25 23 17 25 48 34 29 34 38 42  30 50 58 36 39 28 27 35 30 34  46 46 39 51 46 75 66 20"
 # Mark
 vm_calvin["Mark"]="45 28 35 41 43 56 37 38 51 53  33 44 37 72 47 20"
 # Luke
 vm_calvin["Luke"]="80 52 38 44 39 49 50 56 62 42  54 59 35 35 32 31 37 43 48 47  38 71 56 53"
 # John
 vm_calvin["John"]="51 25 36 54 47 71 53 59 41 42  57 50 38 31 27 33 26 40 42 31  25"
 # Acts
 vm_calvin["Acts"]="26 47 26 37 42 15 60 40 43 48  30 25 52 28 41 40 34 28 40 38  40 30 35 28 27 32 44 31"
 # Romans
 vm_calvin["Rom"]="32 29 30 25 21 23 25 38 33 21  36 21 14 23 33 27"
 # I Corinthians
 vm_calvin["1Cor"]="31 16 22 21 13 20 40 13 27 33  34 31 13 40 58 24"
 # II Corinthians
 vm_calvin["2Cor"]="24 17 18 18 21 18 16 24 15 18  33 21 13"
 # Galatians
 vm_calvin["Gal"]="24 21 29 31 26 18"
 # Ephesians
 vm_calvin["Eph"]="23 22 21 32 33 24"
 # Philippians
 vm_calvin["Phil"]="30 30 21 23"
 # Colossians
 vm_calvin["Col"]="29 23 25 18"
 # I Thessalonians
 vm_calvin["1Thess"]="10 20 13 18 28"
 # II Thessalonians
 vm_calvin["2Thess"]="12 17 18"
 # I Timothy
 vm_calvin["1Tim"]="20 15 16 16 25 21"
 # II Timothy
 vm_calvin["2Tim"]="18 26 17 22"
 # Titus
 vm_calvin["Titus"]="16 15 15"
 # Philemon
  vm_calvin["Phlm"]=" 25"
 # Hebrews
 vm_calvin["Heb"]="14 18 19 16 14 20 28 13 28 39  40 29 25"
 # James
 vm_calvin["Jas"]="27 26 18 17 20"
 # I Peter
 vm_calvin["1Pet"]="25 25 22 19 14"
 # II Peter
 vm_calvin["2Pet"]="21 22 18"
 # I John
 vm_calvin["1John"]="10 29 24 21 21"
 # II John
 vm_calvin["2John"]="13"
 # III John
 vm_calvin["3John"]="15"
 # Jude
  vm_calvin["Jude"]=" 25"
 # Revelation of John
 vm_calvin["Rev"]="20 29 22 11 14 17 17 13 21 11  19 18 18 20 8 21 18 24 21 15  27 21"

 # Build vm (verses max per chapter)
 vm = ""
 split(canon, canon_array)
 for (canonic_book in canon_array) {
    vm = vm " " vm_calvin[canon_array[canonic_book]]
 }
}


if (match(v11n, /[Cc]atholic/)) {
 # Catholic v11n
 # Canon
 canon="Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Tob Jdt Esth 1Macc 2Macc Job Ps Prov Eccl Song Wis Sir Isa Jer Lam Bar Ezek Dan Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev"

 # Numbers of chapters per book
 # OT
 maxchap["Gen"]=50
 maxchap["Exod"]=40
 maxchap["Lev"]=27
 maxchap["Num"]=36
 maxchap["Deut"]=34
 maxchap["Josh"]=24
 maxchap["Judg"]=21
 maxchap["Ruth"]=4
 maxchap["1Sam"]=31
 maxchap["2Sam"]=24
 maxchap["1Kgs"]=22
 maxchap["2Kgs"]=25
 maxchap["1Chr"]=29
 maxchap["2Chr"]=36
 maxchap["Ezra"]=10
 maxchap["Neh"]=13
 maxchap["Tob"]=14
 maxchap["Jdt"]=16
 maxchap["Esth"]=10
 maxchap["1Macc"]=16
 maxchap["2Macc"]=15
 maxchap["Job"]=42
 maxchap["Ps"]=150
 maxchap["Prov"]=31
 maxchap["Eccl"]=12
 maxchap["Song"]=8
 maxchap["Wis"]=19
 maxchap["Sir"]=51
 maxchap["Isa"]=66
 maxchap["Jer"]=52
 maxchap["Lam"]=5
 maxchap["Bar"]=6
 maxchap["Ezek"]=48
 maxchap["Dan"]=14
 maxchap["Hos"]=14
 maxchap["Joel"]=4
 maxchap["Amos"]=9
 maxchap["Obad"]=1
 maxchap["Jonah"]=4
 maxchap["Mic"]=7
 maxchap["Nah"]=3
 maxchap["Hab"]=3
 maxchap["Zeph"]=3
 maxchap["Hag"]=2
 maxchap["Zech"]=14
 maxchap["Mal"]=3

 # NT
 maxchap["Matt"]=28
 maxchap["Mark"]=16
 maxchap["Luke"]=24
 maxchap["John"]=21
 maxchap["Acts"]=28
 maxchap["Rom"]=16
 maxchap["1Cor"]=16
 maxchap["2Cor"]=13
 maxchap["Gal"]=6
 maxchap["Eph"]=6
 maxchap["Phil"]=4
 maxchap["Col"]=4
 maxchap["1Thess"]=5
 maxchap["2Thess"]=3
 maxchap["1Tim"]=6
 maxchap["2Tim"]=4
 maxchap["Titus"]=3
 maxchap["Phlm"]=1
 maxchap["Heb"]=13
 maxchap["Jas"]=5
 maxchap["1Pet"]=5
 maxchap["2Pet"]=3
 maxchap["1John"]=5
 maxchap["2John"]=1
 maxchap["3John"]=1
 maxchap["Jude"]=1
 maxchap["Rev"]=22

 # Other books
 maxchap["Bel"]=1
 maxchap["3Macc"]=7
 maxchap["4Macc"]=18
 maxchap["4Ezra"]=16
 maxchap["1En"]=105


 # Numbers of verse per chapter
 # Genesis
 vm_catholic["Gen"]="31 25 24 26 32 22 24 22 29 32  32 20 18 24 21 16 27 33 38 18  34 24 20 67 34 35 46 22 35 43  54 33 20 31 29 43 36 30 23 23  57 38 34 34 28 34 31 22 33 26"
 # Exodus
 vm_catholic["Exod"]="22 25 22 31 23 30 29 28 35 29  10 51 22 31 27 36 16 27 25 26  37 30 33 18 40 37 21 43 46 38  18 35 23 35 35 38 29 31 43 38"
 # Leviticus
 vm_catholic["Lev"]="17 16 17 35 26 23 38 36 24 20  47 8 59 57 33 34 16 30 37 27  24 33 44 23 55 46 34"
 # Numbers
 vm_catholic["Num"]="54 34 51 49 31 27 89 26 23 36  35 16 33 45 41 35 28 32 22 29  35 41 30 25 19 66 23 31 39 17  54 42 56 29 34 13"
 # Deuteronomy
 vm_catholic["Deut"]="46 37 29 49 33 25 26 20 29 22  32 31 19 29 23 22 20 22 21 20  23 29 26 22 19 19 26 69 28 20  30 52 29 12"
 # Joshua
 vm_catholic["Josh"]="18 24 17 24 15 27 26 35 27 43  23 24 33 15 63 10 18 28 51 9  45 34 16 33"
 # Judges
 vm_catholic["Judg"]="36, 23, 31, 24, 32, 40, 25, 35, 57, 18,  40, 15, 25, 20, 20, 31, 13, 31, 30, 48,  25"
 # Ruth
 vm_catholic["Ruth"]="22 23 18 22"
 # I Samuel
 vm_catholic["1Sam"]="28 36 21 22 12 21 17 22 27 27  15 25 23 52 35 23 58 30 24 42  16 23 28 23 44 25 12 25 11 31  13"
 # II Samuel
 vm_catholic["2Sam"]="27 32 39 12 25 23 29 18 13 19  27 31 39 33 37 23 29 32 44 26  22 51 39 25"
 # I Kings
 vm_catholic["1Kgs"]="53 46 28 20 32 38 51 66 28 29  43 33 34 31 34 34 24 46 21 43  29 54"
 # II Kings
 vm_catholic["2Kgs"]="18 25 27 44 27 33 20 29 37 36   20 22 25 29 38 20 41 37 37 21  26 20 37 20 30"
 # I Chronicles
 vm_catholic["1Chr"]="54 55 24 43 41 66 40 40 44 14  47 41 14 17 29 43 27 17 19 8  30 19 32 31 31 32 34 21 30"
# II Chronicles
 vm_catholic["2Chr"]="18 17 17 22 14 42 22 18 31 19  23 16 23 14 19 14 19 34 11 37  20 12 21 27 28 23 9 27 36 27  21 33 25 33 27 23"
 # Ezra
 vm_catholic["Ezra"]="11 70 13 24 17 22 28 36 15 44"
 # Nehemiah
 vm_catholic["Neh"]="11 20 38 17 19 19 73 18 37 40  36 47 31"
 # Tobit
 vm_catholic["Tob"]="22 14 17 21 23 19 17 21 6 14  19 22 18 15"
 # Judith
 vm_catholic["Jdt"]="16 28 10 15 24 21 32 36 14 23  23 20 20 19 14 25"
 # Esther
 vm_catholic["Esth"]="22 23 15 17 14 14 10 17 32 3"
 # I Macchabees
 vm_catholic["1Macc"]="64 70 60 61 68 63 50 32 73 89  74 54 53 49 41 24"
 # II Maccabees
 vm_catholic["2Macc"]="36 32 40 50 27 31 42 36 29 38  38 46 26 46 39"
 # Job
 vm_catholic["Job"]="22 13 26 21 27 30 21 22 35 22  20 25 28 22 35 22 16 21 29 29  34 30 17 25 14 14 24 28 25 31  40 22 33 37 16 33 24 41 30 32  26 17"
 # Psalms
 vm_catholic["Ps"]="6 12 9 9 13 11 18 10 21 18  7 9 6 7 5 11 15 51 15 10  14 32 6 10 22 12 14 9 11 13  25 11 22 23 28 13 40 23 14 18  14 12 5 27 18 12 10 15 21 24  21 11 7 9 24 14 12 12 18 14  9 13 12 11 14 20 8 36 37 6  24 20 28 23 11 13 21 72 13 20  17 8 19 13 14 17 7 19 53 17  16 16 5 23 11 13 12 9 9 5  9 29 22 35 45 48 43 14 31 7  10 10 9 8 18 19 2 29 176 7  8 9 4 8 5 6 5 6 8 8  3 18 3 3 21 26 9 8 24 14  10 8 12 15 21 10 20 14 9 6"
 # Proverbs
 vm_catholic["Prov"]="33 22 35 27 23 35 27 36 18 32  31 28 25 35 33 33 28 24 29 30  31 29 35 34 28 28 27 28 27 33  31"
 # Ecclesiastes
 vm_catholic["Eccl"]="18 26 22 17 19 12 29 17 18 20  10 14"
 # Song of Solomon
 vm_catholic["Song"]="17 17 11 17 16 12 14 14"
 # Wisdom
 vm_catholic["Wis"]="16 24 19 20 23 25 30 21 19 21  26 27 19 31 19 29 21 25 22"
 # Sirach
 vm_catholic["Sir"]="30 18 31 31 17 37 36 19 18 31  34 18 26 27 20 30 32 33 30 32  28 27 28 34 26 29 30 26 28 25  31 24 33 31 26 31 31 34 35 30  27 25 35 23 26 20 25 25 16 29  30"
 # Isaiah
 vm_catholic["Isa"]="31 22 26 6 30 13 25 24 21 34  16 6 22 32 9 14 14 7 25 6  17 25 18 23 12 21 13 29 24 33  9 20 24 17 10 22 38 22 8 31  29 25 28 28 25 13 15 22 26 11  23 15 12 17 13 12 21 14 21 22  11 12 19 11 25 24"
 # Jeremiah
 vm_catholic["Jer"]="19 37 25 31 31 30 34 23 25 25  23 17 27 22 21 21 27 23 15 18  14 30 40 10 38 24 22 17 32 24  40 44 26 22 19 32 21 28 18 16  18 22 13 30 5 28 7 47 39 46  64 34"
 # # Lamentations
 vm_catholic["Lam"]="22 22 66 22 22"
 # Baruch
 vm_catholic["Bar"]="22 35 38 37 9 72"
 # Ezekiel
 vm_catholic["Ezek"]="28 10 27 17 17 14 27 18 11 22  25 28 23 23 8 63 24 32 14 44  37 31 49 27 17 21 36 26 21 26  18 32 33 31 15 38 28 23 29 49  26 20 27 31 25 24 23 35"
 # Daniel
 vm_catholic["Dan"]="21 49 100 34 30 29 28 27 27 21  45 13 64 43"
 # Hosea
 vm_catholic["Hos"]="9 25 5 19 15 11 16 14 17 15  11 15 15 10"
 # Joel
 vm_catholic["Joel"]="20 27 5 21"
 # Amos
 vm_catholic["Amos"]="15 16 15 13 27 14 17 14 15"
 # Obadiah
 vm_catholic["Obad"]="21"
 # Jonah
 vm_catholic["Jonah"]="16 11 10 11"
 # Micah
 vm_catholic["Mic"]="16 13 12 14 14 16 20"
 # Nahum
 vm_catholic["Nah"]="14 14 19"
 # Habakkuk
 vm_catholic["Hab"]="17 20 19"
 # Zephaniah
 vm_catholic["Zeph"]="18 15 20"
 # Haggai
 vm_catholic["Hag"]="15 23"
 # Zechariah
 vm_catholic["Zech"]="17 17 10 16 11 15 14 23 17 12  17 14 9 21"
 # Malachi
 vm_catholic["Mal"]="14 17 24"

 # Build vm
 # OT vm
 catholic_ot_list="Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr Ezra Neh Tob Jdt Esth 1Macc 2Macc Job Ps Prov Eccl Song Wis Sir Isa Jer Lam Bar Ezek Dan Hos Joel Amos Obad Jonah Mic Nah Hab Zeph Hag Zech Mal"
 vm = ""
 split(catholic_ot_list, catholic_ot_array)
 for (canonic_book in catholic_ot_array) {
     vm = vm " " vm_catholic[catholic_ot_array[canonic_book]]
 }

 # NT vm
 catholic_nt_list="Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev"
 split(catholic_nt_list, catholic_nt_array)
 for (canonic_book in catholic_nt_array) {
     vm = vm " " vm_kjv[catholic_nt_array[canonic_book]]
 }

 canon = catholic_ot_list " " catholic_nt_list
 # end of catholic v11N
}


if (match(v11n, /[Ll][Xx][Xx]/)) {
 # LXX v11n
 # Canon
 canon="Gen Exod Lev Num Deut Josh Judg Ruth 1Sam 2Sam 1Kgs 2Kgs 1Chr 2Chr 1Esd Ezra Neh Esth Jdt Tob 1Macc 2Macc 3Macc 4Macc Ps PrMan Prov Eccl Song Job Wis Sir PssSol Hos Amos Mic Joel Obad Jonah Nah Hab Zeph Hag Zech Mal Isa Jer Bar Lam EpJer Ezek PrAzar Sus Dan Bel 1En Odes Matt Mark Luke John Acts Rom 1Cor 2Cor Gal Eph Phil Col 1Thess 2Thess 1Tim 2Tim Titus Phlm Heb Jas 1Pet 2Pet 1John 2John 3John Jude Rev"

 # Numbers of chapters per book - unchanged
 # OT
 maxchap["Gen"]=50
 maxchap["Exod"]=40
 maxchap["Lev"]=27
 maxchap["Num"]=36
 maxchap["Deut"]=34
 maxchap["Josh"]=24
 maxchap["Judg"]=21
 maxchap["Ruth"]=4
 maxchap["1Sam"]=31
 maxchap["2Sam"]=24
 maxchap["1Kgs"]=22
 maxchap["2Kgs"]=25
 maxchap["1Chr"]=29
 maxchap["2Chr"]=36
 maxchap["1Esd"]=9
 maxchap["Ezra"]=10
 maxchap["Neh"]=13
 maxchap["Esth"]=16
 maxchap["Jdt"]=16
 maxchap["Tob"]=14
 maxchap["1Macc"]=16
 maxchap["2Macc"]=15
 maxchap["3Macc"]=7
 maxchap["4Macc"]=18
 maxchap["Ps"]=151
 maxchap["PrMan"]=1
 maxchap["Prov"]=31
 maxchap["Eccl"]=12
 maxchap["Song"]=8
 maxchap["Job"]=42
 maxchap["Wis"]=19
 maxchap["Sir"]=51
 maxchap["PssSol"]=18
 maxchap["Hos"]=14
 maxchap["Amos"]=9
 maxchap["Mic"]=7
 maxchap["Joel"]=4
 maxchap["Obad"]=1
 maxchap["Jonah"]=4
 maxchap["Nah"]=3
 maxchap["Hab"]=3
 maxchap["Zeph"]=3
 maxchap["Hag"]=2
 maxchap["Zech"]=14
 maxchap["Mal"]=4
 maxchap["Isa"]=66
 maxchap["Jer"]=52
 maxchap["Bar"]=5
 maxchap["Lam"]=5
 maxchap["EpJer"]=1
 maxchap["Ezek"]=48
 maxchap["PrAzar"]=1
 maxchap["Sus"]=1
 maxchap["Dan"]=12
 maxchap["Bel"]=1
 maxchap["1En"]=108
 maxchap["Odes"]=14

 # NT
 # idem kjv

 # Numbers of verse per chapter
 # Genesis
 vm_lxx["Gen"]="31 25 25 26 32 23 24 22 29 32  32 20 18 24 21 16 27 33 39 18  34 24 20 67 34 35 46 22 35 43  55 33 20 31 29 44 36 30 23 23  57 39 34 34 28 34 31 22 33 26"
 # Exodus
 vm_lxx["Exod"]="22 25 22 31 23 30 29 32 35 29  10 51 22 31 27 36 16 27 25 26  37 31 33 18 40 37 21 43 46 38  18 35 23 35 35 40 21 29 23 38"
 # Leviticus
 vm_lxx["Lev"]="17 16 17 35 26 40 38 36 24 20  47 8 59 57 33 34 16 30 37 27  24 33 44 23 55 46 34"
 # Numbers
 vm_lxx["Num"]="54 34 51 49 31 27 89 26 23 36  35 16 34 45 41 50 28 32 22 29  35 41 30 25 18 65 23 31 40 17  54 42 56 29 34 13"
 # Deuteronomy
 vm_lxx["Deut"]="46 37 29 49 33 25 26 20 29 22  32 32 19 29 23 22 20 22 21 20  23 30 26 24 19 19 27 69 29 20  30 52 29 12"
 # Joshua
 vm_lxx["Josh"]="18 24 17 24 16 27 26 35 33 43  23 24 33 15 64 10 18 28 54 9  49 34 16 36"
 # Judges
 vm_lxx["Judg"]="36 23 31 24 32 40 25 35 57 18  40 15 25 20 20 31 13 32 30 48 25"
 # Ruth
 vm_lxx["Ruth"]="22 23 18 22"
 # I Samuel
 vm_lxx["1Sam"]="28 36 21 22 12 21 17 22 27 27  15 25 23 52 35 23 58 30 24 43  16 23 29 23 44 25 12 25 11 32  13"
 # II Samuel
 vm_lxx["2Sam"]="27 32 39 12 26 23 29 18 13 19  27 31 39 33 37 23 29 33 44 26  22 51 41 25"
 # I Kings
 vm_lxx["1Kgs"]="53 71 39 34 32 38 51 66 28 33  44 54 34 31 34 42 24 46 21 43  43 54"
 # II Kings
 vm_lxx["2Kgs"]="22 25 27 44 27 35 20 29 37 36  21 22 25 29 38 20 41 37 37 21  26 20 37 20 30"
 # I Chronicles
 vm_lxx["1Chr"]="54 55 24 43 41 81 40 40 44 14  47 41 14 17 29 43 27 17 19 8  30 19 32 31 31 32 34 21 30"
 # II Chronicles
 vm_lxx["2Chr"]="18 18 17 23 14 42 22 18 31 19  23 16 23 15 19 14 19 34 11 37  20 12 21 27 28 23 9 27 36 27  21 33 25 33 31 31"
 # I Esdras
 vm_lxx["1Esd"]="58 30 24 63 73 34 15 96 55"
 # Ezra
 vm_lxx["Ezra"]="11 70 13 24 17 22 28 36 15 44"
 # Nehemiah
 vm_lxx["Neh"]="11 20 37 23 19 19 73 18 38 40  36 47 31"
 # Esther
 vm_lxx["Esth"]="22 23 15 17 22 14 10 17 35 13  17 7 30 19 24 24"
 # Judith
 vm_lxx["Jdt"]="16 28 10 15 24 21 32 36 14 23  23 20 20 19 14 25"
 # Tobit
 vm_lxx["Tob"]="22 14 17 21 23 19 18 21 6 14  19 22 19 15"
 # I Maccabees
 vm_lxx["1Macc"]="64 70 60 61 68 63 50 32 73 89  74 53 54 49 41 24"
 # II Maccabees
 vm_lxx["2Macc"]="36 32 40 50 27 31 42 36 29 38  38 46 26 46 39"
 # III Maccabees
 vm_lxx["3Macc"]="29 33 30 21 51 41 23"
 # IV Maccabees
 vm_lxx["4Macc"]="35 24 21 26 38 35 25 29 32 21  27 20 27 20 32 25 24 24"
 # Psalms
 vm_lxx["Ps"]="6 13 9 9 13 11 18 10 40 8  9 6 7 6 11 15 51 15 10 14  32 6 10 22 12 14 9 11 13 25  11 22 23 28 13 40 23 14 18 14  12 6 27 18 12 10 15 21 23 21  11 7 9 24 14 12 12 19 14 9  13 12 11 14 20 8 36 37 7 24  20 28 23 11 13 21 72 13 20 17  8 19 13 14 17 7 19 53 17 16  16 5 23 11 13 12 9 9 5 8  29 22 36 45 48 43 14 31 7 10  10 9 26 18 19 2 29 176 7 8  9 4 8 5 7 5 6 8 8 3  18 3 3 21 26 9 8 24 15 10  8 12 15 22 10 11 20 14 9 6  7"
 # Prayer of Manasses
 vm_lxx["PrMan"]="15"
 # Proverbs
 vm_lxx["Prov"]="35 23 38 28 23 40 28 37 25 33  31 31 27 36 38 33 30 24 29 30  31 31 36 77 31 29 29 30 49 35  31"
 # Ecclesiastes
 vm_lxx["Eccl"]="18 26 22 17 20 12 30 17 18 20  10 14"
 # Song of Solomon
 vm_lxx["Song"]="17 17 11 16 17 13 14 15"
 # Job
 vm_lxx["Job"]="22 18 26 21 27 30 22 22 35 22  20 25 28 22 35 23 16 21 29 29  34 30 17 25 6 14 23 28 25 31  40 22 33 37 16 34 24 41 35 32  34 22"
 # Wisdom
 vm_lxx["Wis"]="16 25 19 20 24 27 30 21 19 21  27 27 19 31 19 29 21 25 22"
 # Sirach
 vm_lxx["Sir"]="30 18 31 31 15 37 36 19 18 31  34 18 26 27 20 30 32 33 31 32  28 27 28 34 26 29 30 26 28 40  31 26 33 31 26 31 31 35 35 30  27 27 33 24 26 20 25 25 16 29  30"
 # Psalms of Solomon
 vm_lxx["PssSol"]="8 41 16 29 22 9 10 40 20 9  9 8 12 10 15 15 51 14"
 # Hosea
 vm_lxx["Hos"]="11 25 5 19 15 12 16 14 17 15  12 15 16 10"
 # Amos
 vm_lxx["Amos"]="15 16 15 13 27 15 17 14 15"
 # Micah
 vm_lxx["Mic"]="16 13 12 14 15 16 20"
 # Joel
 vm_lxx["Joel"]="20 32 21 21"
 # Obadiah
 vm_lxx["Obad"]="21"
 # Jonah
 vm_lxx["Jonah"]="17 11 10 11"
 # Nahum
 vm_lxx["Nah"]="15 14 19"
 # Habakkuk
 vm_lxx["Hab"]="17 20 19"
 # Zephaniah
 vm_lxx["Zeph"]="18 15 21"
 # Haggai
 vm_lxx["Hag"]="15 24"
 # Zechariah
 vm_lxx["Zech"]="21 17 11 14 11 15 14 23 17 12  17 14 9 21"
 # Malachi
 vm_lxx["Mal"]="14 17 24 6"
 # Isaiah
 vm_lxx["Isa"]="31 22 26 6 30 13 25 23 21 34  16 6 22 32 9 14 14 7 25 6  17 25 18 23 12 21 13 29 24 33  9 20 24 17 10 22 38 22 8 31  29 25 28 28 26 13 15 22 26 11  23 15 12 17 13 12 21 14 21 22  11 12 20 12 25 24"
 # Jeremiah
 vm_lxx["Jer"]="19 37 25 31 31 30 34 23 26 25  23 17 27 22 21 21 27 23 15 18  14 30 42 10 39 28 46 64 31 33  47 44 24 22 19 32 24 40 44 26  22 22 32 30 28 28 16 44 38 46  63 34"
 # Baruch
 vm_lxx["Bar"]="22 35 38 37 9"
 # Lamentations
 vm_lxx["Lam"]="22 22 66 22 22"
 # Epistle of Jeremiah
 vm_lxx["EpJer"]="73"
 # Ezekiel
 vm_lxx["Ezek"]="28 13 27 17 17 14 27 18 11 22  25 28 23 23 8 63 24 32 14 49  37 31 49 27 17 21 36 26 21 26  18 32 33 31 15 38 28 23 29 49  26 20 27 31 25 24 23 35"
 # Prayer of Azariah
 vm_lxx["PrAzar"]="68"
 # Susanna
 vm_lxx["Sus"]="64"
 # Daniel
 vm_lxx["Dan"]="21 49 100 37 31 29 28 27 27 21  45 13"
 # Bel and the Dragon
 vm_lxx["Bel"]="42"
 # I Enoch
 vm_lxx["1En"]="9 3 1 1 10 8 6 4 11 22  2 6 10 25 12 4 8 16 3 8  10 14 4 6 7 6 5 3 2 3  3 6 4 3 1 4 6 6 14 10  9 3 4 1 6 8 4 10 4 5  5 9 7 10 4 8 3 6 3 25  13 16 12 2 12 3 13 5 30 4  17 37 8 17 9 14 9 17 6 8  10 20 11 6 10 6 4 3 77 43  19 17 14 11 7 8 10 16 16 13  9 11 15 13 2 19 3 15"
 # Odes
 vm_lxx["Odes"]="19 43 10 20 20 19 45 88 79 88  55 32 79 46"

# NT
# Matthew
 vm_lxx["Matt"]="25 23 17 25 48 34 29 34 38 42  30 50 58 36 39 28 27 35 30 34  46 46 39 51 46 75 66 20"
 # Mark
 vm_lxx["Mark"]="45 28 35 41 43 56 37 38 50 52  33 44 37 72 47 20"
 # Luke
 vm_lxx["Luke"]="80 52 38 44 39 49 50 56 62 42  54 59 35 35 32 31 37 43 48 47  38 71 56 53"
 # John
 vm_lxx["John"]="52 25 36 54 47 71 53 59 41 42  57 50 38 31 27 33 26 40 42 31  25"
 # Acts
 vm_lxx["Acts"]="26 47 26 37 42 15 60 40 43 48  30 25 52 28 41 40 34 28 41 38  40 30 35 27 27 32 44 31"
 # Romans
 vm_lxx["Rom"]="32 29 31 25 21 23 25 39 33 21  36 21 14 26 33 27"
 # I Corinthians
 vm_lxx["1Cor"]="31 16 23 21 13 20 40 13 27 33  34 31 13 40 58 24"
 # II Corinthians
 vm_lxx["2Cor"]="24 17 18 18 21 18 16 24 15 18  33 21 14"
 # Galatians
 vm_lxx["Gal"]="24 21 29 31 26 18"
 # Ephesians
 vm_lxx["Eph"]="23 22 21 32 33 24"
 # Philippians
 vm_lxx["Phil"]="30 30 21 23"
 # Colossians
 vm_lxx["Col"]="29 23 25 18"
 # I Thessalonians
 vm_lxx["1Thess"]="10 20 13 18 28"
 # II Thessalonians
 vm_lxx["2Thess"]="12 17 18"
 # I Timothy
 vm_lxx["1Tim"]="20 15 16 16 25 21"
 # II Timothy
 vm_lxx["2Tim"]="18 26 17 22"
 # Titus
 vm_lxx["Titus"]="16 15 15"
 # Philemon
 vm_lxx["Phlm"]="25"
 # Hebrews
 vm_lxx["Heb"]="14 18 19 16 14 20 28 13 28 39  40 29 25"
 # James
 vm_lxx["Jas"]="27 26 18 17 20"
 # I Peter
 vm_lxx["1Pet"]="25 25 22 19 14"
 # II Peter
 vm_lxx["2Pet"]="21 22 18"
 # I John
 vm_lxx["1John"]="10 29 24 21 21"
 # II John
 vm_lxx["2John"]="13"
 # III John
 vm_lxx["3John"]="15"
 # Jude
 vm_lxx["Jude"]="25"
 # Revelation of John
 vm_lxx["Rev"]="20 29 22 11 14 17 17 13 21 11  19 18 18 20 9 21 18 24 21 15  27 21"

 # Build vm (verses max per chapter)
 vm = ""
 split(canon, canon_array)
 for (canonic_book in canon_array) {
    vm = vm " " vm_lxx[canon_array[canonic_book]]
 }
}






# With the canon variable and list[] array, we generate 2 variables and 1 array:
#  osisNames: contains the list of Osis Names
#  srcBooks : contains list of abbreviations found in the incoming text
#  srcAbbrv[] : contains the Osis Name matching an abbreviation

# Divide osisList and store pieces in osisArray -> osisArray[1]="Gen" and so on
split(canon, canon_array)
for (canonic_book in canon_array) {
   # Generating osisNames
   osisNames = osisNames "^" canon_array[canonic_book] "$|"
   # Divide abbreviations list for each book
   split(list[canon_array[canonic_book]], abbrevList)
   for (nickname in abbrevList) {
     # Generating books
     srcBooks = srcBooks "^" abbrevList[nickname] "$|"
     # Generating srcAbbrv[]
     srcAbbrv[abbrevList[nickname]] = canon_array[canonic_book]
   }
}
# Remove unwanted trailing |
osisNames = substr(osisNames, 1, length(osisNames) - 1)
srcBooks =  substr(srcBooks, 1, length(srcBooks) - 1)

## print "vm=", vm

# With the last 2 arrays maxchap[] and vm[], we generate 2 other arrays:
#  vmax[]     : contains number of verses per chapter, for all books from Ge to 1En
#  startchap[]: hold the index in vmax corresponding to a given chapter of a book
#
# The idea is to return the number of verses for a given book and chapter
# example: vmax[startchap["Heb"]+1] returns the number of verses of Hebrew 1
# -> vmax[startchap["Heb"]+1] = 14

# Generating vmax
split(vm,vmax," ")

# Generating startchap
split(canon, roll," ")
sumchap=0
for (i in roll) {
  startchap[roll[i]]=sumchap+maxchap[roll[i-1]]
  sumchap=startchap[roll[i]]
}
##print "Ref=" vmax[startchap["Rev"]+1] > "/dev/stderr" # for debugging

# 3. Other Variables
# List of separators that separates words
sep="[ |\\-|\\.|,|:|;|\\(|\\)|<|>|=|\\?]"

# Verse regexp
verse_regex = "/^[0-9]{1,3}$/"

# templates for SWORD reference tags
t_plain="<reference osisRef=\"%s.%s.%s\">%s"
t_range="<reference osisRef=\"%s.%s.%s-%s.%s.%s\">%s"
t_short="<reference osisRef=\"%s.%s.%s\">%s</reference>"
t_close="%s</reference>"
if (modtype == "TEI") {
t_plain="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s\">%s"
t_range="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s-%s.%s.%s\">%s"
t_short="<xr type=\"Bible\"><ref osisRef=\"%s.%s.%s\">%s</ref></xr>"
t_close="%s</ref></xr>"
}

# error management
stop="F"
warn="W"
info="I"
# fatal error=1 -> process stopped
fatal=0
# error=1 -> display error
error=0

}




#
# Functions
#

# Function to validate a reference
function validref(foundBook,chapter,verse)
{
 REFOK=match(foundBook,osisNames)
 if (REFOK) {
  chapter=chapter+0
  verse=verse+0
  if (chapter < 1) REFOK=0
  else {
   if (verse < 1) REFOK=0
   else {
    if (chapter > maxchap[foundBook]){
      print foundBook, chapter " > " foundBook, maxchap[foundBook] " max!" > "/dev/stderr"
     REFOK=0
    }
    else {
     if (verse > vmax[startchap[foundBook]+chapter]) {
      print foundBook, chapter ":" verse " > "  foundBook, chapter ":" vmax[startchap[foundBook]+chapter] " max!" > "/dev/stderr"
      REFOK=0
     }
    }
   }
  }
 }
 else printf "«%s» is not a valid OSIS name\n", foundBook > "/dev/stderr"
 return REFOK
}

# Function to save errors encoutered during process
function save_error(gravity,type,start,end)
{
 # gravity  : stop, warn, info
 # type     : no_chapter  : book alone, no chapter found
 #            full_chapter: book+chapter only found, no verses
 #            no_verse    : book, chapter and separator found, no verses
 #            isolated    : number found without preceding book
 #            lower_verse : verse lower than previous one
 #            integrity   : modified text different than original

 # Add information do debug msg
 # start, end: indexes of dbg_info[]
 dbg_info[start]=sprintf("[%s-%s:%s",gravity, type, dbg_info[start])
 dbg_info[end]=sprintf("%s]", dbg_info[end])
 # allow error to be displayed
 error++
 # fatal error
 if (gravity == stop) {
  fatal++
  display_fatal++
 }
}

#
# Main
#

function ref2osis(src_text) {
 # 1. Scan a line and divise string into words
 # 1.1 Reset variables
 len=length(src_text)
 mod_text = ""
 mode="text"
 nblocks=1
 word[nblocks]=""
 dbg_info[nblocks]=""
 last_block=0
 end_verse="0"
 error=0
 display_fatal=0
 # 1.2 Scan line
 for (i=1; i<=len; i++) {
  # Extract char
  char=substr(src_text, i, 1)
  # Search for a separator
  if (match(char,sep))
   new_mode="separator"
  else
   new_mode="text"
  # Increment n
  if (new_mode != mode){
   mode=new_mode
   nblocks++
   word[nblocks]=""
   dbg_info[nblocks]=""
  }
  # Fill in data
  word[nblocks]=word[nblocks]char
  dbg_info[nblocks]=dbg_info[nblocks]char
 }
 # 2. Search for a biblical reference
 for ( i=1 ; i<=nblocks ; i++ ) {
  # 2.1 Full reference
  # Search for a book name
  if (match(word[i],srcBooks)) {
   # Book name found
   foundBook=srcAbbrv[word[i]]
   # Search for a chapter number
   if (i+2 <= nblocks) {
    if (match(word[i+2],/^[0-9]{1,3}$/)) {
     # Chapter number found
     stt_chapter = word[i+2]
     end_chapter = stt_chapter
     last_block = i + 2
     # Search for a verse number
     if (i+4 <= nblocks) {
	 if (match(word[i+3], ":|,")) {
       if (match(word[i+4],/^[0-9]{1,3}[a-f]{0,1}$/)) {
        # Verse number found (ex: Ge 12:4)
        stt_verse = word[i+4] + 0
        end_verse = stt_verse
        last_block= i + 4
        UNIQ=1
        # Search for a verses range
        if (i+6 <= nblocks) {
         if (word[i+5] == "-") {
          if (match(word[i+6],/^[0-9]{1,3}[a-f]{0,1}$/)) {
           # Range verse found (ex: Ge 12:4-5)
           end_verse = word[i+6] + 0
           last_block= i + 6
           # Search for an extended range (ex: Ge 12:17-13:1)
           if (i+8 <= nblocks) {
	    if (match(word[i+7], ":|,")) {
             if (match(word[i+8],/^[0-9]{1,3}[a-f]{0,1}$/)) {
              # Extended range found
              end_chapter = word[i+6]
              end_verse = word[i+8] + 0
              last_block= i + 8
             }
             else save_error(info,"no_verse",i,last_block)
            } # Continue: Extended range
           } # Continue: End of line found
            UNIQ=0
          } # Continue: No Range verse
          else save_error(info,"no_verse",i,last_block)
         } # Continue: No Range verse
        } # Continue: End of line found
        # Insert osisRef
        if (UNIQ) {
         # one reference
         if (validref(foundBook,stt_chapter,stt_verse)) {
          word[i]=sprintf(t_plain,foundBook,end_chapter,stt_verse,word[i])
          word[last_block]=sprintf(t_close,word[last_block])
         }
         else save_error(info,"not_in_v11n",i,last_block)
        }
        else {
         # range of references
         if (validref(foundBook,stt_chapter,stt_verse) && validref(foundBook,end_chapter,end_verse)) {
          if (end_chapter - stt_chapter < 2) {
           word[i]=sprintf(t_range,foundBook,stt_chapter,stt_verse,foundBook,end_chapter,end_verse,word[i])
           word[last_block]=sprintf(t_close,word[last_block])
          }
         }
         else save_error(info,"not_in_v11n",i,last_block)
        }
       }
       else save_error(info,"no_verse",i,last_block)
      }
      else {
       last_block++
       save_error(info,"full_chapter",i,last_block)
      }
     } # Continue: End of line found
     else save_error(info,"no_chapter",i,i+2)
    }
   } # Continue: End of line found
  } # Continue: No book found

  # 2.2 Partial references following a full reference
  # Search for a single number (ex: 22)
  if (i == last_block+2) {
   if (match(word[i],/^[0-9]{1,3}$/)) {
    # Simple verse found
    stt_chapter=end_chapter
    stt_verse=word[i]
    last_block=i
    UNIQ=1
    # Search for range of verses (ex: 25-28)
    if (+2 <= nblocks) {
     if (word[i+1] == "-") {
      if (match(word[i+2],/^[0-9]{1,3}[a-f]{0,1}$/)) {
       # Range of verses found
       end_verse = word[i+2] + 0
       last_block= i + 2
       UNIQ=0
      }
      else save_error(info,"no_verse",i,last_block)
     } # Continue: No Range of verses
     # Search for a reference with chapter (ex: 23:19)
     if (match(word[i+1], ":|,")) {
      if (word[i+2]+0 > 0) {
       # Reference with chapter found
       stt_chapter = word[i]
       end_chapter = stt_chapter
       stt_verse = word[i+2]
       last_block= i + 2
       UNIQ=1
       # Search for a reference with chapter and range of verses (ex: 23:19-21)
       if (i+4 <= nblocks) {
        if (word[i+3] == "-") {
         if (match(word[i+4],/^[0-9]{1,3}[a-f]{0,1}$/)) {
          # Reference with chapter and range of verses found
          end_verse = word[i+4] + 0
          last_block = i + 4
          UNIQ=0
         }
         else save_error(info,"no_verse",i,last_block)
        } # Continue: No reference with chapter and range of verses
       } # Continue: End of line found
      }
      else save_error(info,"no_verse",i,last_block)
     } # Continue: No reference with chapter
     # Insert osisRef
     if (UNIQ) {
      # unique reference
      if (validref(foundBook,stt_chapter,stt_verse)) {
       word[i]=sprintf(t_plain,foundBook,end_chapter,stt_verse,word[i])
       word[last_block]=sprintf(t_close,word[last_block])
      }
      else save_error(info,"not_in_v11n",i,last_block)
     }
     else {
      # range of references
      if (validref(foundBook,stt_chapter,stt_verse) && validref(foundBook,end_chapter,end_verse)) {
       if (end_chapter - stt_chapter < 2) {
        word[i]=sprintf(t_range,foundBook,stt_chapter,stt_verse,foundBook,end_chapter,end_verse,word[i])
        word[last_block]=sprintf(t_close,word[last_block])
       }
      }
      else save_error(info,"not_in_v11n",i,last_block)
     } # End of insert
    } # Continue: End of line found
   } # Continue: Word[i] is not a number
  } # Continue: End of line found
  else {
   if (match(word[i],/^[0-9]{1,3}$/)) {
    if (i > last_block+2) {
     # Alone number, max verse number is 176 - Psalm 119
     if ((word[i]+0 <= 176) && (! match(word[i-2],srcBooks))) {
      if ((word[i-1] == ":") || (word[i+1] == ":"))
       level=warn
      else level=info
       save_error(level,"isolated",i,i)
     }
    }
   }
  }
 }

 # 3. Check integrity
 # 3.1 Rebuild the full string
 check=""
 for ( i=1 ; i<=nblocks ; i++ ) {
  check=check""word[i]
 }

 # 3.2 Remove added SWORD links
 if (modtype == "TEI") {
  gsub(/<xr type=.Bible.>/,"",check)
  gsub(/<ref osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<ref osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}-[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<\/ref><\/xr>/,"",check)
 }
 else {
  gsub(/<reference osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<reference osisRef=.[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}-[0-9]?[A-Z][a-z]{1,5}.[0-9]{1,3}.[0-9]{1,3}.>/,"",check)
  gsub(/<\/reference>/,"",check)
 }

 # 3.3 Check result against the initial value (src_text)
 if (check != src_text) {save_error(stop,"integrity",1,n)}

 # 4. Print
 # 4.1 Regular ouptput
 for ( i=1 ; i<=nblocks ; i++ ) {
#  printf "%s", word[i]
   mod_text = mod_text word[i]
 }
 #printf "\n"

 # 4.2 Debugging information
 if (error) {
  if (debug) {
   printf "Line=%s:\n",NR  > "/dev/stderr"
   for ( i=1 ; i<=nblocks ; i++ ) {
    printf "%s", dbg_info[i]  > "/dev/stderr"
   }
   printf "\n"  > "/dev/stderr"
  }
  if (display_fatal) {
   printf "[  -original:%s]\n", src_text > "/dev/stderr"
   printf "[  -computed:%s]\n", check > "/dev/stderr"
  }
 }
  return (mod_text)
}

END{
 # Quit on fatal errors
 if (fatal) {exit 1}
}


