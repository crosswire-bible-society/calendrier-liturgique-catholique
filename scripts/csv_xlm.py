#!/usr/bin/env python3
import re, sys, glob, os

try: rep=sys.argv[1]
except IndexError: rep='.'

rec1=re.compile('"(.*)","(.*)","(.*)","(.*) "')
rec2=re.compile('[A-Z]')

def impr(g,tit,item,fic_i):
   global liste_en_cours
   tits=item.split()
   if tits[0]=='Messe':
      k=rec2.search(item[1:])
      if k:
         label=item[:k.start(0)]
         item=item[k.start(0)+1:]
         if liste_en_cours: g.write('</list>\n')
         g.write('<item>%s</item>\n<list>'%(label))
         liste_en_cours=True
      else: sys.stderr.write("\n%s => Item non conforme :\n%s\n"%(fic_i,item))
   g.write('<item>%s%s</item>\n'%(tit,item))

def csv2xml(fic_i,fic_o):
   global liste_en_cours
   with open(fic_i,'r') as f, open(fic_o,'w') as g:
      for lig in f:
         liste_en_cours=False
         k=rec1.match(lig)
         if k: dat,ferie,coul,items=k.groups()
         else: sys.stderr.write("\n%s => Ligne non conforme :\n%s\n"%(fic_i,lig)); continue
         mois_jour='.'.join(dat.split('-')[1:])
         g.write('$$$%s\n<div type="entry" osisID="%s">\n'%(mois_jour,mois_jour))
         if   coul=="vert":   n_subsec=""
         elif coul=="blanc":  n_subsec="1"
         elif coul=="violet": n_subsec="2"
         elif coul=="rouge":  n_subsec="3"
         elif coul=="rouge~blanc":  n_subsec="4"
         elif coul=="blanc~rouge":  n_subsec="5"
         else:
            n_subsec="?"
            sys.stderr.write("\n%s => Couleur non prévue : %s\n%s\n"%(fic_i,coul,lig));
         g.write('<title>%s</title><div type="subSection%s">%s</div>\n'%(ferie,n_subsec,coul))
         items=list(map(lambda x:x.strip(),items.split('/')))
         lon=len(items)
         if lon==3: tit=('Première lecture ', 'Psaume ', 'Évangile ')
         else:      tit=('Première lecture ', 'Psaume ', 'Deuxième lecture ', 'Évangile ')
         i=0
         g.write('<list>\n')
         for item in items:
            try: tititem="%s: "%tit[i]
            except IndexError: tititem=''
            impr(g,tititem,item,fic_i); i+=1
         if liste_en_cours: g.write('</list>\n')
         g.write('</list></div>\n')

for fic_i in glob.glob(os.path.join(rep,'*.csv')):
   fic_o=re.sub('\.csv$','.xml',fic_i)
   sys.stderr.write("fic_i=%s\nfic_o=%s\n"%(fic_i,fic_o))
   csv2xml(fic_i,fic_o)
